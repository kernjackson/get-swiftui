//
//  SwiftUIView.swift
//  Get SwiftUI
//
//  Created by Kern Jackson on 6/11/19.
//  Copyright © 2019. All rights reserved.
//

#if canImport(SwiftUI)
import SwiftUI

@available(iOS 13.0, *)
struct TestSwiftUIView : View {
    var body: some View {
        Text("Hello SwiftUI!")
    }
}
#endif

//#if DEBUG
//struct TestSwiftUIView_Previews : PreviewProvider {
//    @available(iOS 12.0, *)
//    static var previews: some View {
//        TestSwiftUIView()
//    }
//}
//#endif
