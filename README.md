# Get SwiftUI in iOS 12

This example project presents a SwiftUI view on iOS 13, and a UIKit view on iOS 12.

- Set Deployment to iOS 12
- Link SwiftUI in Project -> Target -> Build Phases -> ```SwiftUI.framework```
- Set the status for SwiftUI.framework to optional
- Add ```var window: UIWindow?``` to AppDelegate
- Wrap UIKit code with ```#if canImport(SwiftUI) #endif``` and
- Apply ```@available(iOS 13.0, *)``` liberally

I have no idea how well this works. Only that it copiles and runs on both iOS 12 and iOS 13 devices.