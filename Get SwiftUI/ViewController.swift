//
//  ViewController.swift
//  Not SwiftUI
//
//  Created by Kern Jackson on 6/11/19.
//  Copyright © 2019. All rights reserved.
//

import UIKit
import SwiftUI

class ViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        #if canImport(SwiftUI)
        if #available(iOS 13.0, *) {
            getSwiftUI()
        } else {
            // Fallback on earlier versions
            print("iOS 12")
        }
        #endif
    }
    
    #if canImport(SwiftUI)
    @available(iOS 13.0, *)
    func getSwiftUI() {
        let testSwiftUIView = TestSwiftUIView() // swiftUIView is View
        let vc = UIHostingController(rootView: testSwiftUIView)
        self.present(vc, animated: true, completion: nil)
    }
    #endif
    
}
